require 'mail'

mail = Mail.new do
    from     'sample_mail'
    to       'your_mail'
    subject  'subject'
    body  'text_body'
end
mail.delivery_method :sendmail
mail.deliver
