require 'watir'
require_relative 'credentials'

username = $username
password = $password

puts "Apro GitLab"
browser = Watir::Browser.new :chrome
browser.goto "https://gitlab.com/users/sign_in"

puts "Apro GitHub"
browser.a(:id => 'oauth-login-github').click

browser.text_field(:name => "login").set "#{username}"
browser.text_field(:name => "password").set "#{password}"

puts "Accesso a GitHub"
browser.input(:class => 'btn btn-primary btn-block').click

sleep (15)
